<?php

namespace App\Models\Competition;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RunnerTime extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $table = 'competition_runner_time';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = ['runner_id', 'competition_id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'runner_id',
        'competition_id',
        'begin',
        'end',
        'seconds',
    ];

    protected $dates = [
        'begin',
        'end',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        // auto-sets values on creation
        static::creating(function ($model) {
            $time = $model->begin->diff($model->end);
            $model->seconds = $time->s + ($time->i * 60) + ($time->h * 3600) + ($time->days * 86400);
        });

        static::updating(function ($model) {
            $time = $model->begin->diff($model->end);
            $model->seconds = $time->s + ($time->i * 60) + ($time->h * 3600) + ($time->days * 86400);
        });
    }


    /**
     * User Profile Relationships.
     *
     * @var array
     */
    public function competitionRunner()
    {
        return $this->hasOne(\App\Models\Competition\Runner::class, ['runner_id', 'competition_id'], ['runner_id', 'competition_id']);
    }

    /**
     * @return array
     */
    public static function rules()
    {
        $rules = [
            'runner_id' => 'required|integer',
            'competition_id' => 'required|integer',
            'begin' => 'required|date_format:H:i:s',
            'end' => 'required|date_format:H:i:s',
        ];
        return $rules;
    }

    /**
     * @return array
     */
    public static function messages()
    {
        return [
            'begin.greater_than_field' => 'Start time should be less than the end.'
        ];
    }
}
