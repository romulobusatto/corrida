<?php

namespace App\Models\Competition;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Runner extends Model
{
    use HasFactory;


    public $incrementing = false;

    protected $table = 'competition_runner';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = ['runner_id', 'competition_id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'runner_id',
        'competition_id',
    ];

    /**
     * User Profile Relationships.
     *
     * @var array
     */
    public function runner()
    {
        return $this->hasOne(\App\Models\Runner::class, 'id', 'runner_id');
    }

    /**
     * User Profile Relationships.
     *
     * @var array
     */
    public function competition()
    {
        return $this->hasOne(\App\Models\Competition::class, 'id', 'competition_id');
    }
}
