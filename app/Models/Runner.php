<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Runner extends Model
{
    use HasFactory;

    protected $table = 'runners';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'cpf',
        'birth_date',
    ];

    protected $dates = [
        'birth_date',
    ];

    /**
     * The roles that belong to the user.
     */
    public function competitions()
    {
        return $this->belongsToMany(Competition::class, 'competition_runner', 'runner_id', 'competition_id')
            ->withTimestamps();
    }

    /**
     * @return array
     */
    public static function rules()
    {
        $rules = [
            'name' => 'required|min:3|max:255',
            'birth_date' => 'required|date_format:Y-m-d|before:-18 years',
            'cpf' => 'required|formato_cpf|cpf',
        ];
        return $rules;
    }

    /**
     * @return array
     */
    public static function messages()
    {
        return [
            'birth_date.before' => 'Runner must be of legal age.',
        ];
    }
}
