<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Competition extends Model
{
    use HasFactory;

    protected $table = 'competitions';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date',
    ];

    protected $dates = [
        'date',
    ];

    /**
     * User Profile Relationships.
     *
     * @var array
     */
    public function type()
    {
        return $this->hasOne(Type::class, 'id', 'type_id');
    }

    /**
     * The users that belong to the role.
     */
    public function runners()
    {
        return $this->belongsToMany(Runner::class, 'competition_runner', 'competition_id', 'runner_id')
            ->withTimestamps();
    }

    /**
     * @return array
     */
    public static function rules()
    {
        $rules = [
            'date' => 'required|date_format:Y-m-d H:i:s',
            'type_id' => 'required|exists:types,id',
        ];
        return $rules;
    }

    /**
     * @return array
     */
    public static function messages()
    {
        return [
            'type_id.required' => 'Competition type not found.',
        ];
    }
}
