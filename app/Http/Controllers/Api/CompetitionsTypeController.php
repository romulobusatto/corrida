<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class CompetitionsTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipos = \App\Models\Type::select('id', 'name')->orderBy('id','asc')->get();
        return response()->json($tipos);
    }


}
