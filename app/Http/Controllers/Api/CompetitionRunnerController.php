<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Competition;
use App\Models\Runner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompetitionRunnerController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'runner_id' => 'required|exists:runners,id',
            'competition_id' => 'required|exists:competitions,id',
        ];
        $this->validate($request, $rules);

        $competitionId = $request->get('competition_id');
        $runnerId = $request->get('runner_id');

        $competition = Competition::find($competitionId);

        if ($this->haveAnotherCompetitionSameDay($competition, $runnerId)) {
            return response()->json([
                'message' => 'Insertion not allowed.',
                'errors' => [
                    'Runner has another competition the same day.',
                ]
            ], 401);
        }

        $competition->runners()->sync($runnerId, false);
        $return = $competition->toArray();

        $return['type'] = $competition->type;
        $return['runners'] = $competition->runners()->get();

        return $return;
    }

    /**
     * @param Competition $competition
     * @param $runnerId
     */
    protected function haveAnotherCompetitionSameDay(Competition $competition, $runnerId)
    {
        $select = Runner::select('competitions.id')
            ->join('competition_runner', 'competition_runner.runner_id', '=', 'runners.id')
            ->join('competitions', 'competition_runner.competition_id', '=', 'competitions.id');

        $select->where([
            ['competitions.id', '<>', $competition->id],
            [DB::raw('DATE_FORMAT(competitions.date, "%Y-%m-%d")'), '=', $competition->date->format('Y-m-d')],
            ['runners.id', '=', $runnerId],
        ]);

        return $select->get()->count() > 0;
    }

}
