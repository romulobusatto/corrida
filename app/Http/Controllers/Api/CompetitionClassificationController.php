<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Competition\RunnerTime;
use Illuminate\Support\Facades\DB;

class CompetitionClassificationController extends Controller
{
    const
        AGE_18_25 = '18_25',
        AGE_26_35 = '26_35',
        AGE_36_45 = '36_45',
        AGE_46_55 = '46_55',
        AGE_MORE_55 = 'more_55';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function general()
    {
        $list = $this->getListClassification();
        return response()->json($this->mountClassificationByCompetition($list));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function age()
    {
        $list = $this->getListClassification();
        $list = $this->mountClassificationByCompetition($list);
        return response()->json($this->mountClassificationByCompetitionAndAge($list));
    }

    /**
     * Returns list ordered by competition and runner time.
     * @return array
     */
    private function getListClassification()
    {
        $select = RunnerTime::select('competition_runner_time.begin', 'competition_runner_time.end',
            'competition_runner_time.seconds', 'competition_runner_time.competition_id',
            'competition_runner_time.runner_id', 'types.name as type_name', 'runners.name as runner_name',
            'competitions.date as competition_date',
            DB::raw('year(from_days(to_days(now())-to_days(runners.birth_date))) as age'))
            ->join('competitions', 'competition_runner_time.competition_id', '=', 'competitions.id')
            ->join('runners', 'competition_runner_time.runner_id', '=', 'runners.id')
            ->join('types', 'competitions.type_id', '=', 'types.id')
            ->orderBy('competitions.date', 'ASC')
            ->orderBy('competition_runner_time.competition_id', 'ASC')
            ->orderBy('competition_runner_time.seconds', 'ASC')
            ->orderBy(DB::raw('year(from_days(to_days(now())-to_days(runners.birth_date)))'), 'ASC');

        return $select->get()->toArray();
    }

    /**
     * Assembles general classification of the competition.
     * @param $list
     * @return array
     */
    private function mountClassificationByCompetition($list)
    {
        $return = [];
        $position = 0;
        $lastTime = 0;
        foreach ($list as $runnerTime) {
            if (!isset($return[$runnerTime['competition_id']])) {
                $position = 0;
                $return[$runnerTime['competition_id']] = [
                    'competition_id' => $runnerTime['competition_id'],
                    'date' => $runnerTime['competition_date'],
                    'type' => $runnerTime['type_name'],
                    'classification' => []
                ];
            }

            if ($runnerTime['seconds'] != $lastTime) {
                $lastTime = $runnerTime['seconds'];
                $position++;
            }

            $return[$runnerTime['competition_id']]['classification'][] = [
                'runner_id' => $runnerTime['runner_id'],
                'name' => $runnerTime['runner_name'],
                'age' => $runnerTime['age'],
                'begin' => $runnerTime['begin'],
                'end' => $runnerTime['end'],
                'seconds' => $runnerTime['seconds'],
                'position' => $position,
            ];
        }
        return array_values($return);
    }

    /**
     * @param $competitions
     * @return mixed
     */
    private function mountClassificationByCompetitionAndAge($competitions)
    {
        foreach ($competitions as &$competition) {
            $groupClassification = [
                self::AGE_18_25 => [
                    'grupo' => '18 - 25 anos',
                    'classification' => [],
                    'qtdPositions' => 0,
                    'lastTime' => 0,
                ],
                self::AGE_26_35 => [
                    'grupo' => '26 - 35 anos',
                    'classification' => [],
                    'qtdPositions' => 0,
                    'lastTime' => 0,
                ],
                self::AGE_36_45 => [
                    'grupo' => '36 - 45 anos',
                    'classification' => [],
                    'qtdPositions' => 0,
                    'lastTime' => 0,
                ],
                self::AGE_46_55 => [
                    'grupo' => '46 - 55 anos',
                    'classification' => [],
                    'qtdPositions' => 0,
                    'lastTime' => 0,
                ],
                self::AGE_MORE_55 => [
                    'grupo' => 'Acima de 55 anos',
                    'classification' => [],
                    'qtdPositions' => 0,
                    'lastTime' => 0,
                ],
            ];

            foreach ($competition['classification'] as $runner) {
                $groupKey = $this->getGroup($runner);
                $group = &$groupClassification[$groupKey];

                if ($runner['seconds'] != $group['lastTime']) {
                    $group['lastTime'] = $runner['seconds'];
                    $group['qtdPositions']++;
                }
                $runner['position'] = $group['qtdPositions'];
                $group['classification'][] = $runner;
            }

            $competition['classification'] = array_values($groupClassification);
        }
        return $competitions;
    }


    /**
     * Group returns from age.
     * @param $runner
     * @return string
     */
    private function getGroup($runner)
    {
        if ($runner['age'] <= 25) {
            return self::AGE_18_25;
        }
        if ($runner['age'] <= 35) {
            return self::AGE_26_35;
        }
        if ($runner['age'] <= 45) {
            return self::AGE_36_45;
        }
        if ($runner['age'] <= 55) {
            return self::AGE_46_55;
        }
        return self::AGE_MORE_55;
    }

}
