<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Competition\RunnerTime;
use App\Models\Competition\Runner as CompetitionRunner;
use Illuminate\Http\Request;
use DateTime;

class CompetitionRunnerTimeController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, RunnerTime::rules(), RunnerTime::messages());

        $competitionId = $request->get('competition_id');
        $runnerId = $request->get('runner_id');
        $begin = DateTime::createFromFormat('H:i:s', $request->get('begin'));
        $end = DateTime::createFromFormat('H:i:s', $request->get('end'));

        if ($end < $begin) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'end' => 'Final time must be longer than the initial one.',
                ]
            ], 401);
        }

        $competitionRunner = CompetitionRunner::select()
            ->where([
                ['runner_id', '=', $runnerId],
                ['competition_id', '=', $competitionId]
            ])->get()->first();

        if (!$competitionRunner) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'Runner not found in the competition.',
                ]
            ], 404);
        }

        return RunnerTime::create([
            'runner_id' => $competitionRunner->runner_id,
            'competition_id' => $competitionRunner->competition_id,
            'begin' => $begin,
            'end' => $end
        ])->toArray();
    }
}
