<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Competition;

class CompetitionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provas = Competition::with('type')
            ->orderBy('date','desc')
            ->orderBy('id','asc')
            ->get(['id', 'date', 'type_id']);

        return response()->json($provas);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Competition::rules(), Competition::messages());

        $competition = new Competition();
        $competition->date = $request->get('date');
        $competition->type_id = $request->get('type_id');
        try {
            $competition->save();
        } catch (\Exception $e) {
            if (strpos($e->getMessage(), 'uk_competitions_type_id_date') !== false) {
                return response()->json([
                    'message' => 'Insertion not allowed.',
                    'errors' => [
                        'There is already a competition of the same type registered for the schedule.',
                    ]
                ], 401);
            }
        }
        return $competition->toArray();
    }

}
