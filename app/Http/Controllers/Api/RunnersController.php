<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Runner;

class RunnersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $runners = Runner::select('id', 'name', 'cpf', 'birth_date')
            ->orderBy('name', 'asc')->get();

        return response()->json($runners);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Runner::rules(), Runner::messages());

        $runner = new Runner();
        $runner->birth_date = $request->get('birth_date');
        $runner->name = $request->get('name');
        $runner->cpf = $request->get('cpf');
        try {
            $runner->save();
        } catch (\Exception $e) {
            if (strpos($e->getMessage(), 'uk_corredor_cpf') !== false) {
                return response()->json([
                    'message' => 'Insertion not allowed.',
                    'errors' => [
                        'There is already a registered runner with this CPF.',
                    ]
                ], 401);
            }
        }
        return $runner->toArray();
    }
}
