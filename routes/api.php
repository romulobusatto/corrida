<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('competitions/type', '\App\Http\Controllers\Api\CompetitionsTypeController@index');
Route::get('competitions', '\App\Http\Controllers\Api\CompetitionsController@index');
Route::post('competition', '\App\Http\Controllers\Api\CompetitionsController@store');

Route::get('runners', '\App\Http\Controllers\Api\RunnersController@index');
Route::post('runner', '\App\Http\Controllers\Api\RunnersController@store');

Route::post('competition/runner', '\App\Http\Controllers\Api\CompetitionRunnerController@store');
Route::post('competition/runner/time', '\App\Http\Controllers\Api\CompetitionRunnerTimeController@store');

Route::get('competition/classification/age', '\App\Http\Controllers\Api\CompetitionClassificationController@age');
Route::get('competition/classification/general', '\App\Http\Controllers\Api\CompetitionClassificationController@general');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
