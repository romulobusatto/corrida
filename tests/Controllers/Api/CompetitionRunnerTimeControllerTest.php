<?php

namespace Tests\Controllers\Api;

use App\Models\Competition;
use App\Models\Competition\Runner as CompetitionRunner;
use App\Models\Runner;
use Illuminate\Foundation\Testing\RefreshDatabase;
use function Psy\debug;
use Tests\TestCase;

class CompetitionRunnerTimeControllerTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreWithIvalidData1()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/competition/runner/time', []);

        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'runner_id' => ['The runner id field is required.'],
                'competition_id' => ['The competition id field is required.'],
                'begin' => ['The begin field is required.'],
                'end' => ['The end field is required.'],
            ]
        ]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreWithIvalidData2()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/competition/runner/time', [
            'runner_id' => 'a',
            'competition_id' => 'b',
            'begin' => '26:00:00',
            'end' => 'aa:kk:00',
        ]);

        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'runner_id' => ['The runner id must be an integer.'],
                'competition_id' => ['The competition id must be an integer.'],
                'begin' => ['The begin does not match the format H:i:s.'],
                'end' => ['The end does not match the format H:i:s.'],
            ]
        ]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreWithIvalidData3()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/competition/runner/time', [
            'runner_id' => '1',
            'competition_id' => '1',
            'begin' => '12:00:00',
            'end' => '11:00:00',
        ]);

        $response->assertStatus(401);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'end' => 'Final time must be longer than the initial one.',
            ]
        ]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreCompetitionRunnerNotFound()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/competition/runner/time', [
            'runner_id' => '1',
            'competition_id' => '1',
            'begin' => '12:00:00',
            'end' => '13:00:00',
        ]);

        $response->assertStatus(404);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'Runner not found in the competition.',
            ]
        ]);
    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreOk()
    {
        $competitionRunner = CompetitionRunner::factory()->create();

        $dataBegin = \DateTime::createFromFormat('d/m/Y H:i:s', date('d/m/Y') . ' 07:00:00');
        $dataEnd = \DateTime::createFromFormat('d/m/Y H:i:s', date('d/m/Y') . ' 07:58:13');

        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/competition/runner/time', [
            'runner_id' => $competitionRunner->runner_id,
            'competition_id' => $competitionRunner->competition_id,
            'begin' => $dataBegin->format('H:i:s'),
            'end' => $dataEnd->format('H:i:s'),
        ]);

        $response->assertStatus(200);
        $content = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('runner_id', $content);
        $this->assertArrayHasKey('competition_id', $content);
        $this->assertArrayHasKey('begin', $content);
        $this->assertArrayHasKey('end', $content);
        $this->assertArrayHasKey('seconds', $content);


        $this->assertEquals($competitionRunner->runner_id, $content['runner_id']);
        $this->assertEquals($competitionRunner->competition_id, $content['competition_id']);
        $this->assertEquals($dataBegin->format('Y-m-d\TH:i:s.000000\Z'), $content['begin']);
        $this->assertEquals($dataEnd->format('Y-m-d\TH:i:s.000000\Z'), $content['end']);
        $this->assertEquals(3493, $content['seconds']);

    }

}
