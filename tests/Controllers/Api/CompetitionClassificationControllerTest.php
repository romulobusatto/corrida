<?php

namespace Tests\Controllers\Api;

use App\Models\Competition;
use App\Models\Runner;
use App\Models\Type;
use App\Models\Competition\Runner as CompetitionRunner;
use App\Models\Competition\RunnerTime as CompetitionRunnerTime;
use Illuminate\Foundation\Testing\RefreshDatabase;
use function Psy\debug;
use Tests\TestCase;
use DateTime;
use DateInterval;

class CompetitionClassificationControllerTest extends TestCase
{

    use RefreshDatabase;

    public function testAge()
    {
        $this->createData();
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->get('/api/competition/classification/age', []);

        $response->assertStatus(200);

        $response->assertJson([
            [
                "competition_id" => 1,
                "date" => "2021-01-20 07:00:00",
                "type" => "3km",
                "classification" => [
                    [
                        "grupo" => "18 - 25 anos",
                        "classification" => [
                            [
                                "runner_id" => 2,
                                "name" => "Runner 2",
                                "age" => 19,
                                "begin" => "2021-01-20T07:00:00.000000Z",
                                "end" => "2021-01-20T08:09:59.000000Z",
                                "seconds" => 4199,
                                "position" => 1
                            ],
                            [
                                "runner_id" => 1,
                                "name" => "Runner 1",
                                "age" => 18,
                                "begin" => "2021-01-20T07:00:00.000000Z",
                                "end" => "2021-01-20T08:10:00.000000Z",
                                "seconds" => 4200,
                                "position" => 2
                            ],
                            [
                                "runner_id" => 3,
                                "name" => "Runner 3",
                                "age" => 20,
                                "begin" => "2021-01-20T07:00:00.000000Z",
                                "end" => "2021-01-20T08:10:02.000000Z",
                                "seconds" => 4202,
                                "position" => 3
                            ]
                        ],
                        "qtdPositions" => 3,
                        "lastTime" => 4202
                    ],
                    [
                        "grupo" => "26 - 35 anos",
                        "classification" => [
                            [
                                "runner_id" => 5,
                                "name" => "Runner 5",
                                "age" => 26,
                                "begin" => "2021-01-20T07:00:00.000000Z",
                                "end" => "2021-01-20T08:14:02.000000Z",
                                "seconds" => 4442,
                                "position" => 1
                            ],
                            [
                                "runner_id" => 6,
                                "name" => "Runner 6",
                                "age" => 31,
                                "begin" => "2021-01-20T07:00:00.000000Z",
                                "end" => "2021-01-20T08:14:02.000000Z",
                                "seconds" => 4442,
                                "position" => 1
                            ]
                        ],
                        "qtdPositions" => 1,
                        "lastTime" => 4442
                    ],
                    [
                        "grupo" => "36 - 45 anos",
                        "classification" => [
                            [
                                "runner_id" => 8,
                                "name" => "Runner 8",
                                "age" => 36,
                                "begin" => "2021-01-20T07:00:00.000000Z",
                                "end" => "2021-01-20T08:12:02.000000Z",
                                "seconds" => 4322,
                                "position" => 1
                            ],
                            [
                                "runner_id" => 9,
                                "name" => "Runner 9",
                                "age" => 45,
                                "begin" => "2021-01-20T07:00:00.000000Z",
                                "end" => "2021-01-20T08:15:02.000000Z",
                                "seconds" => 4502,
                                "position" => 2
                            ]
                        ],
                        "qtdPositions" => 2,
                        "lastTime" => 4502
                    ],
                    [
                        "grupo" => "46 - 55 anos",
                        "classification" => [
                        ],
                        "qtdPositions" => 0,
                        "lastTime" => 0
                    ],
                    [
                        "grupo" => "Acima de 55 anos",
                        "classification" => [
                            [
                                "runner_id" => 11,
                                "name" => "Runner 11",
                                "age" => 56,
                                "begin" => "2021-01-20T07:00:00.000000Z",
                                "end" => "2021-01-20T08:18:02.000000Z",
                                "seconds" => 4682,
                                "position" => 1
                            ],
                            [
                                "runner_id" => 13,
                                "name" => "Runner 13",
                                "age" => 60,
                                "begin" => "2021-01-20T07:00:00.000000Z",
                                "end" => "2021-01-20T08:19:02.000000Z",
                                "seconds" => 4742,
                                "position" => 2
                            ]
                        ],
                        "qtdPositions" => 2,
                        "lastTime" => 4742
                    ]
                ]
            ],
            [
                "competition_id" => 2,
                "date" => "2021-01-20 07:00:00",
                "type" => "5km",
                "classification" => [
                    [
                        "grupo" => "18 - 25 anos",
                        "classification" => [
                            [
                                "runner_id" => 4,
                                "name" => "Runner 4",
                                "age" => 25,
                                "begin" => "2021-01-20T07:00:00.000000Z",
                                "end" => "2021-01-20T08:12:02.000000Z",
                                "seconds" => 4322,
                                "position" => 1
                            ]
                        ],
                        "qtdPositions" => 1,
                        "lastTime" => 4322
                    ],
                    [
                        "grupo" => "26 - 35 anos",
                        "classification" => [
                            [
                                "runner_id" => 7,
                                "name" => "Runner 7",
                                "age" => 35,
                                "begin" => "2021-01-20T07:00:00.000000Z",
                                "end" => "2021-01-20T08:18:02.000000Z",
                                "seconds" => 4682,
                                "position" => 1
                            ]
                        ],
                        "qtdPositions" => 1,
                        "lastTime" => 4682
                    ],
                    [
                        "grupo" => "36 - 45 anos",
                        "classification" => [
                        ],
                        "qtdPositions" => 0,
                        "lastTime" => 0
                    ],
                    [
                        "grupo" => "46 - 55 anos",
                        "classification" => [
                            [
                                "runner_id" => 10,
                                "name" => "Runner 10",
                                "age" => 55,
                                "begin" => "2021-01-20T07:00:00.000000Z",
                                "end" => "2021-01-20T08:15:02.000000Z",
                                "seconds" => 4502,
                                "position" => 1
                            ]
                        ],
                        "qtdPositions" => 1,
                        "lastTime" => 4502
                    ],
                    [
                        "grupo" => "Acima de 55 anos",
                        "classification" => [
                            [
                                "runner_id" => 12,
                                "name" => "Runner 12",
                                "age" => 57,
                                "begin" => "2021-01-20T07:00:00.000000Z",
                                "end" => "2021-01-20T08:16:02.000000Z",
                                "seconds" => 4562,
                                "position" => 1
                            ]
                        ],
                        "qtdPositions" => 1,
                        "lastTime" => 4562
                    ]
                ]
            ]
        ]);
    }

    public function testGeneral()
    {
        $this->createData();
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->get('/api/competition/classification/general', []);

        $response->assertStatus(200);

        $response->assertJson([
            [
                "competition_id" => 29,
                "date" => "2021-01-20 07:00:00",
                "type" => "3km",
                "classification" => [
                    [
                        "runner_id" => 41,
                        "name" => "Runner 2",
                        "age" => 19,
                        "begin" => "2021-01-20T07:00:00.000000Z",
                        "end" => "2021-01-20T08:09:59.000000Z",
                        "seconds" => 4199,
                        "position" => 1
                    ],
                    [
                        "runner_id" => 40,
                        "name" => "Runner 1",
                        "age" => 18,
                        "begin" => "2021-01-20T07:00:00.000000Z",
                        "end" => "2021-01-20T08:10:00.000000Z",
                        "seconds" => 4200,
                        "position" => 2
                    ],
                    [
                        "runner_id" => 42,
                        "name" => "Runner 3",
                        "age" => 20,
                        "begin" => "2021-01-20T07:00:00.000000Z",
                        "end" => "2021-01-20T08:10:02.000000Z",
                        "seconds" => 4202,
                        "position" => 3
                    ],
                    [
                        "runner_id" => 47,
                        "name" => "Runner 8",
                        "age" => 36,
                        "begin" => "2021-01-20T07:00:00.000000Z",
                        "end" => "2021-01-20T08:12:02.000000Z",
                        "seconds" => 4322,
                        "position" => 4
                    ],
                    [
                        "runner_id" => 44,
                        "name" => "Runner 5",
                        "age" => 26,
                        "begin" => "2021-01-20T07:00:00.000000Z",
                        "end" => "2021-01-20T08:14:02.000000Z",
                        "seconds" => 4442,
                        "position" => 5
                    ],
                    [
                        "runner_id" => 45,
                        "name" => "Runner 6",
                        "age" => 31,
                        "begin" => "2021-01-20T07:00:00.000000Z",
                        "end" => "2021-01-20T08:14:02.000000Z",
                        "seconds" => 4442,
                        "position" => 5
                    ],
                    [
                        "runner_id" => 48,
                        "name" => "Runner 9",
                        "age" => 45,
                        "begin" => "2021-01-20T07:00:00.000000Z",
                        "end" => "2021-01-20T08:15:02.000000Z",
                        "seconds" => 4502,
                        "position" => 6
                    ],
                    [
                        "runner_id" => 50,
                        "name" => "Runner 11",
                        "age" => 56,
                        "begin" => "2021-01-20T07:00:00.000000Z",
                        "end" => "2021-01-20T08:18:02.000000Z",
                        "seconds" => 4682,
                        "position" => 7
                    ],
                    [
                        "runner_id" => 52,
                        "name" => "Runner 13",
                        "age" => 60,
                        "begin" => "2021-01-20T07:00:00.000000Z",
                        "end" => "2021-01-20T08:19:02.000000Z",
                        "seconds" => 4742,
                        "position" => 8
                    ]
                ]
            ],
            [
                "competition_id" => 30,
                "date" => "2021-01-20 07:00:00",
                "type" => "5km",
                "classification" => [
                    [
                        "runner_id" => 43,
                        "name" => "Runner 4",
                        "age" => 25,
                        "begin" => "2021-01-20T07:00:00.000000Z",
                        "end" => "2021-01-20T08:12:02.000000Z",
                        "seconds" => 4322,
                        "position" => 1
                    ],
                    [
                        "runner_id" => 49,
                        "name" => "Runner 10",
                        "age" => 55,
                        "begin" => "2021-01-20T07:00:00.000000Z",
                        "end" => "2021-01-20T08:15:02.000000Z",
                        "seconds" => 4502,
                        "position" => 2
                    ],
                    [
                        "runner_id" => 51,
                        "name" => "Runner 12",
                        "age" => 57,
                        "begin" => "2021-01-20T07:00:00.000000Z",
                        "end" => "2021-01-20T08:16:02.000000Z",
                        "seconds" => 4562,
                        "position" => 3
                    ],
                    [
                        "runner_id" => 46,
                        "name" => "Runner 7",
                        "age" => 35,
                        "begin" => "2021-01-20T07:00:00.000000Z",
                        "end" => "2021-01-20T08:18:02.000000Z",
                        "seconds" => 4682,
                        "position" => 4
                    ]
                ]
            ]
        ]);
    }

    public function createData()
    {
        $type3km = Type::factory()->create(['name' => '3km']);
        $type5km = Type::factory()->create(['name' => '5km']);

        $competition3km = Competition::factory()->create(['type_id' => $type3km->id, 'date' => DateTime::createFromFormat('d/m/Y H:i:s', '20/01/2021 07:00:00')]);
        $competition5km = Competition::factory()->create(['type_id' => $type5km->id, 'date' => DateTime::createFromFormat('d/m/Y H:i:s', '20/01/2021 07:00:00')]);

        $runnerAge18 = Runner::factory()->create(['name' => 'Runner 1', 'birth_date' => $this->getBirthDateByAge(18)]);
        $runnerAge19 = Runner::factory()->create(['name' => 'Runner 2', 'birth_date' => $this->getBirthDateByAge(19)]);
        $runnerAge20 = Runner::factory()->create(['name' => 'Runner 3', 'birth_date' => $this->getBirthDateByAge(20)]);
        $runnerAge25 = Runner::factory()->create(['name' => 'Runner 4', 'birth_date' => $this->getBirthDateByAge(25)]);
        $runnerAge26 = Runner::factory()->create(['name' => 'Runner 5', 'birth_date' => $this->getBirthDateByAge(26)]);
        $runnerAge31 = Runner::factory()->create(['name' => 'Runner 6', 'birth_date' => $this->getBirthDateByAge(31)]);
        $runnerAge35 = Runner::factory()->create(['name' => 'Runner 7', 'birth_date' => $this->getBirthDateByAge(35)]);
        $runnerAge36 = Runner::factory()->create(['name' => 'Runner 8', 'birth_date' => $this->getBirthDateByAge(36)]);
        $runnerAge45 = Runner::factory()->create(['name' => 'Runner 9', 'birth_date' => $this->getBirthDateByAge(45)]);
        $runnerAge55 = Runner::factory()->create(['name' => 'Runner 10', 'birth_date' => $this->getBirthDateByAge(55)]);
        $runnerAge56 = Runner::factory()->create(['name' => 'Runner 11', 'birth_date' => $this->getBirthDateByAge(56)]);
        $runnerAge57 = Runner::factory()->create(['name' => 'Runner 12', 'birth_date' => $this->getBirthDateByAge(57)]);
        $runnerAge60 = Runner::factory()->create(['name' => 'Runner 13', 'birth_date' => $this->getBirthDateByAge(60)]);

        $cr3kmR18Age = CompetitionRunner::factory()->create(['competition_id' => $competition3km->id, 'runner_id' => $runnerAge18->id]);
        $cr3kmR19Age = CompetitionRunner::factory()->create(['competition_id' => $competition3km->id, 'runner_id' => $runnerAge19->id]);
        $cr3kmR20Age = CompetitionRunner::factory()->create(['competition_id' => $competition3km->id, 'runner_id' => $runnerAge20->id]);
        $cr3kmR26Age = CompetitionRunner::factory()->create(['competition_id' => $competition3km->id, 'runner_id' => $runnerAge26->id]);
        $cr3kmR31Age = CompetitionRunner::factory()->create(['competition_id' => $competition3km->id, 'runner_id' => $runnerAge31->id]);
        $cr3kmR36Age = CompetitionRunner::factory()->create(['competition_id' => $competition3km->id, 'runner_id' => $runnerAge36->id]);
        $cr3kmR45Age = CompetitionRunner::factory()->create(['competition_id' => $competition3km->id, 'runner_id' => $runnerAge45->id]);
        $cr3kmR56Age = CompetitionRunner::factory()->create(['competition_id' => $competition3km->id, 'runner_id' => $runnerAge56->id]);
        $cr3kmR60Age = CompetitionRunner::factory()->create(['competition_id' => $competition3km->id, 'runner_id' => $runnerAge60->id]);

        CompetitionRunnerTime::factory()->create([
            'competition_id' => $cr3kmR18Age->competition_id, 'runner_id' => $cr3kmR18Age->runner_id,
            'begin' => $this->getDateTime('07:00:00'), 'end' => $this->getDateTime('08:10:00')
        ]);
        CompetitionRunnerTime::factory()->create([
            'competition_id' => $cr3kmR19Age->competition_id, 'runner_id' => $cr3kmR19Age->runner_id,
            'begin' => $this->getDateTime('07:00:00'), 'end' => $this->getDateTime('08:09:59')
        ]);
        CompetitionRunnerTime::factory()->create([
            'competition_id' => $cr3kmR20Age->competition_id, 'runner_id' => $cr3kmR20Age->runner_id,
            'begin' => $this->getDateTime('07:00:00'), 'end' => $this->getDateTime('08:10:02')
        ]);
        CompetitionRunnerTime::factory()->create([
            'competition_id' => $cr3kmR26Age->competition_id, 'runner_id' => $cr3kmR26Age->runner_id,
            'begin' => $this->getDateTime('07:00:00'), 'end' => $this->getDateTime('08:14:02')
        ]);
        CompetitionRunnerTime::factory()->create([
            'competition_id' => $cr3kmR31Age->competition_id, 'runner_id' => $cr3kmR31Age->runner_id,
            'begin' => $this->getDateTime('07:00:00'), 'end' => $this->getDateTime('08:14:02')
        ]);
        CompetitionRunnerTime::factory()->create([
            'competition_id' => $cr3kmR36Age->competition_id, 'runner_id' => $cr3kmR36Age->runner_id,
            'begin' => $this->getDateTime('07:00:00'), 'end' => $this->getDateTime('08:12:02')
        ]);
        CompetitionRunnerTime::factory()->create([
            'competition_id' => $cr3kmR45Age->competition_id, 'runner_id' => $cr3kmR45Age->runner_id,
            'begin' => $this->getDateTime('07:00:00'), 'end' => $this->getDateTime('08:15:02')
        ]);
        CompetitionRunnerTime::factory()->create([
            'competition_id' => $cr3kmR56Age->competition_id, 'runner_id' => $cr3kmR56Age->runner_id,
            'begin' => $this->getDateTime('07:00:00'), 'end' => $this->getDateTime('08:18:02')
        ]);
        CompetitionRunnerTime::factory()->create([
            'competition_id' => $cr3kmR60Age->competition_id, 'runner_id' => $cr3kmR60Age->runner_id,
            'begin' => $this->getDateTime('07:00:00'), 'end' => $this->getDateTime('08:19:02')
        ]);

        $cr3kmR25Age = CompetitionRunner::factory()->create(['competition_id' => $competition5km->id, 'runner_id' => $runnerAge25->id]);
        $cr3kmR35Age = CompetitionRunner::factory()->create(['competition_id' => $competition5km->id, 'runner_id' => $runnerAge35->id]);
        $cr3kmR55Age = CompetitionRunner::factory()->create(['competition_id' => $competition5km->id, 'runner_id' => $runnerAge55->id]);
        $cr3kmR57Age = CompetitionRunner::factory()->create(['competition_id' => $competition5km->id, 'runner_id' => $runnerAge57->id]);

        CompetitionRunnerTime::factory()->create([
            'competition_id' => $cr3kmR25Age->competition_id, 'runner_id' => $cr3kmR25Age->runner_id,
            'begin' => $this->getDateTime('07:00:00'), 'end' => $this->getDateTime('08:12:02')
        ]);
        CompetitionRunnerTime::factory()->create([
            'competition_id' => $cr3kmR35Age->competition_id, 'runner_id' => $cr3kmR35Age->runner_id,
            'begin' => $this->getDateTime('07:00:00'), 'end' => $this->getDateTime('08:18:02')
        ]);
        CompetitionRunnerTime::factory()->create([
            'competition_id' => $cr3kmR55Age->competition_id, 'runner_id' => $cr3kmR55Age->runner_id,
            'begin' => $this->getDateTime('07:00:00'), 'end' => $this->getDateTime('08:15:02')
        ]);
        CompetitionRunnerTime::factory()->create([
            'competition_id' => $cr3kmR57Age->competition_id, 'runner_id' => $cr3kmR57Age->runner_id,
            'begin' => $this->getDateTime('07:00:00'), 'end' => $this->getDateTime('08:16:02')
        ]);

        return [
            'types' => [
                'type3km' => $type3km,
                'type5km' => $type5km,
            ],
            'competitions' => [
                'competition3km' => $competition3km,
                'competition5km' => $competition5km,
            ],
            'runners' => [
                'runnerAge18' => $runnerAge18,
                'runnerAge19' => $runnerAge19,
                'runnerAge20' => $runnerAge20,
                'runnerAge25' => $runnerAge25,
                'runnerAge26' => $runnerAge26,
                'runnerAge31' => $runnerAge31,
                'runnerAge35' => $runnerAge35,
                'runnerAge36' => $runnerAge36,
                'runnerAge45' => $runnerAge45,
                'runnerAge55' => $runnerAge55,
                'runnerAge56' => $runnerAge56,
                'runnerAge57' => $runnerAge57,
                'runnerAge60' => $runnerAge60,
            ]
        ];
    }

    private function getBirthDateByAge(int $age)
    {
        $data = new DateTime();
        $data->sub(new DateInterval("P{$age}Y"));
        return $data;
    }

    private function getDateTime($time)
    {
        return DateTime::createFromFormat('d/m/Y H:i:s', "20/01/2021 {$time}");
    }
}
