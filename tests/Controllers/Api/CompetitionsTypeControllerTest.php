<?php

namespace Tests\Controllers\Api;

use App\Models\Type;
use Illuminate\Foundation\Testing\RefreshDatabase;
use function Psy\debug;
use Tests\TestCase;

class CompetitionsTypeControllerTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndexTestEmpty()
    {
        $response = $this->get('/api/competitions/type');

        $response->assertStatus(200);
        $response->assertJsonCount(0);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndexTest()
    {
        $tipo1 = Type::factory()->create();
        $tipo2 = Type::factory()->create();
        $tipo3 = Type::factory()->create();
        $tipo4 = Type::factory()->create();
        $tipo5 = Type::factory()->create();

        $response = $this->get('/api/competitions/type');

        $response->assertStatus(200);
        $response->assertJsonCount(5);
        $response->assertJson([
            ['id' => $tipo1->id, 'name' => $tipo1->name],
            ['id' => $tipo2->id, 'name' => $tipo2->name],
            ['id' => $tipo3->id, 'name' => $tipo3->name],
            ['id' => $tipo4->id, 'name' => $tipo4->name],
            ['id' => $tipo5->id, 'name' => $tipo5->name],
        ]);
    }
}
