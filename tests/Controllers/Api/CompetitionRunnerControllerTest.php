<?php

namespace Tests\Controllers\Api;

use App\Models\Competition;
use App\Models\Runner;
use Illuminate\Foundation\Testing\RefreshDatabase;
use function Psy\debug;
use Tests\TestCase;

class CompetitionRunnerControllerTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreWithIvalidData1()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/competition/runner', []);

        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'runner_id' => ['The runner id field is required.'],
                'competition_id' => ['The competition id field is required.'],
            ]
        ]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreWithIvalidData2()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/competition/runner', ['runner_id' => 1, 'competition_id' => 1]);

        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'runner_id' => ['The selected runner id is invalid.'],
                'competition_id' => ['The selected competition id is invalid.'],
            ]
        ]);
    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreOk()
    {
        $runner1 = Runner::factory()->create();
        $competition = Competition::factory()->create();

        //Insere um corredor
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/competition/runner', ['runner_id' => $runner1->id, 'competition_id' => $competition->id]);

        $response->assertStatus(200);

        $content = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('date', $content);
        $this->assertArrayHasKey('type_id', $content);
        $this->assertArrayHasKey('id', $content);
        $this->assertArrayHasKey('type', $content);
        $this->assertArrayHasKey('runners', $content);

        $this->assertEquals($competition->date->format('Y-m-d\TH:i:s.000000\Z'), $content['date']);
        $this->assertEquals($competition->type_id, $content['type_id']);
        $this->assertEquals($competition->id, $content['id']);

        $this->assertCount(1, $content['runners']);
        $this->assertEquals($runner1->id, $content['runners'][0]['id']);


        //Insere segundo corredor
        $runner2 = Runner::factory()->create();
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/competition/runner', ['runner_id' => $runner2->id, 'competition_id' => $competition->id]);

        $response->assertStatus(200);

        $content = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('date', $content);
        $this->assertArrayHasKey('type_id', $content);
        $this->assertArrayHasKey('id', $content);
        $this->assertArrayHasKey('type', $content);
        $this->assertArrayHasKey('runners', $content);

        $this->assertEquals($competition->date->format('Y-m-d\TH:i:s.000000\Z'), $content['date']);
        $this->assertEquals($competition->type_id, $content['type_id']);
        $this->assertEquals($competition->id, $content['id']);

        $this->assertCount(2, $content['runners']);
        $this->assertEquals($runner1->id, $content['runners'][0]['id']);
        $this->assertEquals($runner2->id, $content['runners'][1]['id']);
    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreAnotherCompetitionSameDay()
    {
        $runner1 = Runner::factory()->create();
        $competition1 = Competition::factory()->create(['date' => '2021-01-19 10:20:00']);
        $competition1->runners()->sync($runner1->id, false);

        $competition2 = Competition::factory()->create(['date' => '2021-01-19 10:20:00']);

        //Insere um corredor
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/competition/runner', ['runner_id' => $runner1->id, 'competition_id' => $competition2->id]);

        $response->assertStatus(401);
        $response->assertJson([
            'message' => 'Insertion not allowed.',
            'errors' => [
                'Runner has another competition the same day.'
            ]
        ]);
    }
}
