<?php

namespace Tests\Controllers\Api;

use App\Models\Competition;
use App\Models\Type;
use Illuminate\Foundation\Testing\RefreshDatabase;
use function Psy\debug;
use Tests\TestCase;

class CompetitionsControllerTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndexEmpty()
    {
        $response = $this->get('/api/competitions');

        $response->assertStatus(200);
        $response->assertJsonCount(0);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $prova1 = Competition::factory()->create();
        $prova2 = Competition::factory()->create();

        $response = $this->get('/api/competitions');

        $response->assertStatus(200);
        $response->assertJsonCount(2);

        $response->assertJson([
            [
                'id' => $prova1->id,
                'date' => $prova1->date->format('Y-m-d\TH:i:s.000000\Z'),
                'type_id' => $prova1->type->id,
                'type' => [
                    'id' => $prova1->type->id,
                    'name' => $prova1->type->name,
                    'created_at' => $prova1->type->created_at->format('Y-m-d\TH:i:s.000000\Z'),
                    'updated_at' => $prova1->type->updated_at->format('Y-m-d\TH:i:s.000000\Z'),
                ]
            ],
            [
                'id' => $prova2->id,
                'date' => $prova2->date->format('Y-m-d\TH:i:s.000000\Z'),
                'type_id' => $prova2->type->id,
                'type' => [
                    'id' => $prova2->type->id,
                    'name' => $prova2->type->name,
                    'created_at' => $prova2->type->created_at->format('Y-m-d\TH:i:s.000000\Z'),
                    'updated_at' => $prova2->type->updated_at->format('Y-m-d\TH:i:s.000000\Z'),
                ]
            ],
        ]);
    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreWithIvalidData1()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/competition', []);

        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'date' => ['The date field is required.'],
                'type_id' => ['Competition type not found.'],
            ]
        ]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreWithIvalidData2()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/competition', ['date' => '10/01/2021', 'type_id' => 2]);

        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'date' => ['The date does not match the format Y-m-d H:i:s.'],
                'type_id' => ['The selected type id is invalid.'],
            ]
        ]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreDiplicated()
    {
        $prova1 = Competition::factory()->create();

        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/competition', ['date' => $prova1->date->format('Y-m-d H:i:s'), 'type_id' => $prova1->type_id]);

        $response->assertStatus(401);
        $response->assertJson([
            'message' => 'Insertion not allowed.',
            'errors' => [
                'There is already a competition of the same type registered for the schedule.'
            ]
        ]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreOk()
    {
        $type1 = Type::factory()->create();
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/competition', ['date' => '2021-01-19 10:20:00', 'type_id' => $type1->id]);

        $response->assertStatus(200);

        $content = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('date', $content);
        $this->assertArrayHasKey('type_id', $content);
        $this->assertArrayHasKey('id', $content);

        $this->assertEquals('2021-01-19T10:20:00.000000Z', $content['date']);
        $this->assertEquals($type1->id, $content['type_id']);
        $this->assertNotEmpty($content['id']);
    }
}
