<?php

namespace Tests\Controllers\Api;

use App\Models\Runner;
use Illuminate\Foundation\Testing\RefreshDatabase;
use function Psy\debug;
use Illuminate\Support\Str;
use Tests\TestCase;
use DateTime;
use DateInterval;

class RunnersControllerTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndexEmpty()
    {
        $response = $this->get('/api/runners');

        $response->assertStatus(200);
        $response->assertJsonCount(0);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $tipo1 = Runner::factory()->create(['name' => 'Runner 2']);
        $tipo2 = Runner::factory()->create(['name' => 'Runner 1']);

        $response = $this->get('/api/runners');

        $response->assertStatus(200);
        $response->assertJsonCount(2);

        $response->assertJson([
            [
                'id' => $tipo2->id,
                'name' => $tipo2->name,
                'cpf' => $tipo2->cpf,
                'birth_date' => $tipo2->birth_date->format('Y-m-d\T00:00:00.000000\Z'),
            ],
            [
                'id' => $tipo1->id,
                'name' => $tipo1->name,
                'cpf' => $tipo1->cpf,
                'birth_date' => $tipo1->birth_date->format('Y-m-d\T00:00:00.000000\Z'),
            ],
        ]);
    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreWithIvalidData1()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/runner', []);

        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'name' => ['The name field is required.'],
                'birth_date' => ['The birth date field is required.'],
                'cpf' => ['The cpf field is required.'],
            ]
        ]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreWithIvalidData2()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/runner', ['birth_date' => '10/01/2021', 'name' => 'a', 'cpf' => '123323']);

        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'name' => ['The name must be at least 3 characters.'],
                'birth_date' => ['The birth date does not match the format Y-m-d.', 'Runner must be of legal age.'],
                'cpf' => ['O campo cpf não possui o formato válido de CPF.', 'O campo cpf não é um CPF válido.'],
            ]
        ]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreWithIvalidData3()
    {
        $data = new DateTime();
        $data->sub(new DateInterval('P17Y'));

        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/runner', ['birth_date' => $data->format('Y-m-d'), 'name' => Str::random(256), 'cpf' => '807.753.520-83']);


        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'name' => ['The name may not be greater than 255 characters.'],
                'birth_date' => ['Runner must be of legal age.'],
                'cpf' => ['O campo cpf não é um CPF válido.'],
            ]
        ]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreDiplicated()
    {
        $data = new DateTime();
        $data->sub(new DateInterval('P18Y'));

        $runner = Runner::factory()->create(['cpf' => '807.753.520-80']);

        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/runner', ['birth_date' => $data->format('Y-m-d'), 'name' => Str::random(255), 'cpf' => $runner->cpf]);

        $response->assertStatus(401);
        $response->assertJson([
            'message' => 'Insertion not allowed.',
            'errors' => [
                'There is already a registered runner with this CPF.'
            ]
        ]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStoreOk()
    {
        $data = new DateTime();
        $data->sub(new DateInterval('P18Y'));
        $name = Str::random(255);

        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('/api/runner', [
            'birth_date' => $data->format('Y-m-d'),
            'name' => $name,
            'cpf' => '807.753.520-80'
        ]);

        $response->assertStatus(200);

        $content = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('birth_date', $content);
        $this->assertArrayHasKey('name', $content);
        $this->assertArrayHasKey('cpf', $content);
        $this->assertArrayHasKey('id', $content);

        $this->assertNotEmpty($content['id']);
        $this->assertEquals($name, $content['name']);
        $this->assertEquals('807.753.520-80', $content['cpf']);
        $this->assertEquals($data->format('Y-m-d\T00:00:00.000000\Z'), $content['birth_date']);
    }
}
