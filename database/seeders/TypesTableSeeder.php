<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos = \App\Models\Type::all();
        if($tipos->count() == 0){
            \App\Models\Type::create(['name' => '3 km']);
            \App\Models\Type::create(['name' => '5 km']);
            \App\Models\Type::create(['name' => '10 km']);
            \App\Models\Type::create(['name' => '21 km']);
            \App\Models\Type::create(['name' => '42 km']);
        }
    }
}
