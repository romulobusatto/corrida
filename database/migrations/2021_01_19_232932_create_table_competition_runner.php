<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCompetitionRunner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competition_runner', function (Blueprint $table) {
            $table->unsignedBigInteger('runner_id');
            $table->unsignedBigInteger('competition_id');

            $table->primary(['runner_id', 'competition_id'], 'pk_competition_runner');
            $table->foreign('runner_id', 'fk_competition_runner_runners')
                ->references('id')->on('runners');
            $table->foreign('competition_id', 'fk_competition_runner_competitions')
                ->references('id')->on('competitions');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competition_runner');
    }
}
