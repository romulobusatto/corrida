<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCompetitionRunnerTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competition_runner_time', function (Blueprint $table) {
            $table->unsignedBigInteger('runner_id');
            $table->unsignedBigInteger('competition_id');
            $table->time('begin');
            $table->time('end');
            $table->bigInteger('seconds');

            $table->primary(['runner_id', 'competition_id'], 'pk_competition_runner_time');
            $table->foreign(['runner_id', 'competition_id'], 'fk_competition_runner_time_competition_runner')
                ->references(['runner_id', 'competition_id'])->on('competition_runner');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competition_runner_time');
    }
}
