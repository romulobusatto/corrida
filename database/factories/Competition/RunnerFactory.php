<?php

namespace Database\Factories\Competition;

use App\Models\Competition;
use App\Models\Competition\Runner as CompetitionRunner;
use App\Models\Runner;
use Illuminate\Database\Eloquent\Factories\Factory;

class RunnerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CompetitionRunner::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $runner = Runner::factory()->create();
        $competition = Competition::factory()->create();

        return [
            'runner_id' => $runner->id,
            'competition_id' => $competition->id,
        ];
    }
}
