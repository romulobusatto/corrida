<?php

namespace Database\Factories\Competition;

use App\Models\Competition;
use App\Models\Competition\Runner as CompetitionRunner;
use App\Models\Competition\RunnerTime as CompetitionRunnerRunnerTime;
use Illuminate\Database\Eloquent\Factories\Factory;

class RunnerTimeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CompetitionRunnerRunnerTime::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $competitionRunner = CompetitionRunner::factory()->create();

        return [
            'runner_id' => $competitionRunner->runner_id,
            'competition_id' => $competitionRunner->competition_id,
            'begin' => now(),
            'end' => now(),
        ];
    }
}
