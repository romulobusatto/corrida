<?php

namespace Database\Factories;

use App\Models\Competition;
use App\Models\Type;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompetitionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Competition::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $type = Type::factory()->create();
        return [
            'type_id' => $type->id,
            'date' => now()
        ];
    }
}
