#!/bin/bash

docker-compose up -d
docker container exec Corrida-app composer install
docker container exec Corrida-app php artisan migrate
docker container exec Corrida-app php artisan migrate --env=testing
docker container exec Corrida-app php artisan db:seed
docker container exec Corrida-app php artisan test --testsuite=Controllers
